..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_curriculum:

==========
Curriculum
==========

I am currently pursuing a DSc. degree at the `Computer Graphics Laboratory <lcg_>`_ (LGC), part of
the `Federal University of Rio de Janeiro <ufrj_>`_'s (UFRJ) `Computer and Systems Engineering
Graduate Program <pesc_>`_ (PESC). I expect to obtain my degree by March, 2020. My Lattes CV [1]_ is
`here <lattes_cv_>`_. Below is a summarized view of my qualification and experience. My publications
are listed in the :ref:`section_publications` page.

Qualification
=============

 * **(2013 - 2016) MSc. in Computing and Systems Engineering** - Federal University of Rio de
   Janeiro (UFRJ_, Brazil). Dissertation: Estimating Hand Poses from RGB-D Data. Advised by `Prof.
   Ricardo Guerra Marroquim <page_ricardo_>`_.
 * **(2008 - 2013) Bachelor in Computer Science** - Federal University of Rio de Janeiro (UFRJ_,
   Brazil) Final project: Segmentação e Rotulação de Componenentes Conexos em GPU (Segmentation and
   Connected Component Labeling in GPU). Advised by `Prof. Silvana Rosetto <lattes_silvana_>`_,
   co-advised by `Prof. Marcio Portes de Albuquerque <lattes_marcio_>`_.
 * **(2005 - 2008) Technical course in informatics** (high-school level) - Celso Suckow da Fonseca
   Federal Center for Technological Education (`CEFET/RJ <cefet-rj_>`_, Brazil).

Experience
==========

 * **(2015 - 2017) Teaching staff** in `UFRJ <ufrj_>`_'s department of computer science (DCC_).
   Courses taught: introductory and intermediate programming for Engineering in Python (lecture
   materials in the :ref:`section_teaching` page).
 
Grants
======
 
 * **(2016 - present) Full-time DSc. study grant** by the Brazilian National Council for Scientific and
   Technological Development (CNPq_).
 * **(2013 - 2015) Full-time MSc. study grant** by CNPq_.
 * **(2010 - 2013) Scientific initiation study grant** by CNPq_ (CS bachelor's final project).
 * **(2006 - 2008) Technological initiation study grant** by CNPq_ (studies on artificial life,
   advised by `Prof. Eduardo Bezerra da Silva <page_eduardo_>`_.

----

.. [1]
   The *Lattes Curriculum* is the standard curriculum for all Brazilian researchers, either students
   and professors, funded by the Brazilian Government. More information can be found in it `this
   page <lattes_>`_.

.. _cefet-rj: http://www.cefet-rj.br/
.. _cnpq: http://cnpq.br/
.. _dcc: https://dcc.ufrj.br
.. _lattes: http://lattes.cnpq.br
.. _lattes_cv: http://lattes.cnpq.br/1991114698553806
.. _lattes_silvana: http://lattes.cnpq.br/0054098292730720
.. _lattes_marcio: http://lattes.cnpq.br/2786430796602633
.. _lcg: https://www.lcg.ufrj.br
.. _page_eduardo: http://eic.cefet-rj.br/~ebezerra/
.. _page_ricardo: https://lcg.ufrj.br/~marroquim/
.. _page_silvana: http://www.dcc.ufrj.br/~silvana/
.. _pesc: https://cos.ufrj.br
.. _ufrj: https://ufrj.br

