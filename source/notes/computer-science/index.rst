..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_notes_computer-science:

Computer Science
================

.. toctree::
   :glob:
   :maxdepth: 2

   */index
   *

