..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

=================
Testing in Python
=================

Fundamental tools
=================

* Unit testing with `pytest <https://pytest.org/en/latest/>`_.

  * Coverage measurement with `coverage.py <https://coverage.readthedocs.io>`_ (via `pytest-cov <https://pypi.org/project/pytest-cov/>`_ plugin).

* Testing on multiple Python versions with `tox <https://tox.readthedocs.io/en/latest/>`_.

Interesting links
=================

* Introduction

  * `How to test scientific code <http://pljung.de/posts/testing-scientific-code/>`_, by Philipp Jung
    --- a very simple introduction to why testing automation is a good idea for people developing scientific software;
    only explains the basics of what *unit tests* and *integration tests* are, and how to set up fixtures;
    covers the `Unittest framework <https://docs.python.org/3/library/unittest.html>`_.
    
* Proper configuration

  * `Testing your python package as installed <https://blog.ganssle.io/articles/2019/08/test-as-installed.html>`_ ---
    a nice complement to Ionel's `Packaging a Python library <https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure>`_.
    These posts describe how to approach testing a package as if it were installed (but without having to actually install it in your system).

* Mocking

  * `Understanding the Python mock object library <https://realpython.com/python-mock-library/>`_, by by Alex Ronquillo (at `Real Python`_).
  * `Why your mock doesn't work <https://nedbatchelder.com/blog/201908/why_your_mock_doesnt_work.html>`_, by Ned Batchelder.
  * `Mocking print() in unit tests <https://realpython.com/python-print/#mocking-python-print-in-unit-tests>`_, by Bartosz Zaczyński (at `Real Python`_).
  
* Alternative testing modalities (besides unit testing)

  * Static type checking with `mypy <http://mypy-lang.org/>`_
  * Mutation testing with `mutpy <https://github.com/mutpy/mutpy>`_
  
.. _Real Python: https://realpython.com
