..
   vim: ts=3: sw=3:
   vim: tw=0:
   vim: spell: spelllang=en:

=====================
Programming languages
=====================

* `Reference course`_ given at Brown University in 2012, by Prof. Shriram Krishnamurthi.
  Includes a free online textbook, Youtube classes, and walks you through the creation of an interpreter for a subset of Python.
* `A Brief, Incomplete, and Mostly Wrong History of Programming Languages`_ --- a funny diachronic tale of the development of computer science.

.. _A Brief, Incomplete, and Mostly Wrong History of Programming Languages:
.. _Reference course: http://cs.brown.edu/courses/csci1730/2012/

