import datetime
from IPython.display import display, HTML


def footer():
    return header()


def header():
    return display(HTML('''\
        <div class="custom-header">
            <table>
                <tr>
                    <th style="width: 100px; text-align: center">Created by</td>
                    <td style="width: 100px;text-align: center"><a href="https://www.pedroasad.com">Pedro Asad</a>
                    <td style="width: 88px;" rowspan="2"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
                        <img 
                            alt="Creative Commons CC BY 4.0 license"
                            style="border-width:0"
                            src="https://i.creativecommons.org/l/by/4.0/88x31.png"
                        />
                    </a></td>
                    <td rowspan="2" style="text-align: justify">
                        This work is licensed under
                        <a rel="license"
                           href="http://creativecommons.org/licenses/by/4.0/"
                           > Creative Commons Attribution 4.0 International
                       </a>. 
                       Click the license icon or the link for additional information on this license and
                       similar ones. You may also check out some of my additional notebooks 
                       <a href="https://pedroasad.com/notebooks">here</a>.
                    </td>
                </tr>
                <tr>
                    <th style="text-align: center">Last update</td>
                    <td style="text-align: center">%s</td>
                </tr>
            <table>
        </div>
    ''' % datetime.datetime.now().strftime('%Y-%m-%d (%a) %H:%M')))