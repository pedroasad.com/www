..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_notes_computer-graphics:

=================
Computer Graphics
=================

.. figure:: im-cv-cg-cg.svg
   
   Sub-areas of visual computing, according to :cite:`Velho2003`.

Bibliography
============

.. bibliography:: refs.bib
