..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

This website is a sort of all-in-one solution to the following needs:

 * Showcasing my expertise to fellow researchers and likely employers (see
   :ref:`section_curriculum` and :ref:`section_publications`)
 * Sharing and discussing knowledge with fellow researchers and programmers about various topics
   (see :ref:`section_notes` and :ref:`blog-archives`)
 * Building an open and ever-evolving personal knowledge base (see :ref:`section_notes`)
 * Granting access to lecture materials to past and future students (see :ref:`section_teaching`)
 
After all it's so much easier to keep everything (up-to-date) in one single place.
