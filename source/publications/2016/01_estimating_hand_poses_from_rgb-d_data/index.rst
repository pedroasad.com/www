..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _thesis_2016_estimating_hand_poses_from_rgb-d_data:

Estimating hand poses from RGB-D data
-------------------------------------

* Author: Pedro Asad.
* Advisor: Prof. `Ricardo G. Marroquim <Marroquim_>`_.
* Masters dissertation submitted to PESC_, COPPE_, UFRJ_, on June 2016.
* Keywords: pose estimation, particle swarm optimization, RGB-D cameras.
* :download:`Full text <./document.pdf>`
* Citation in :download:`BibTeX format <./citation.bib>`.

.. _COPPE: https://coppe.ufrj.br
.. _Marroquim: http://graphics.tudelft.nl/~marroquim/
.. _PESC: https://www.cos.ufrj.bro
.. _UFRJ: https://ufrj.br

