..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _paper_2019_on_gpu_connected_components_and_properties:

On GPU Connected Components and Properties
------------------------------------------

* Authors:
   * Pedro Asad
   * Ricardo Marroquim
   * Andréa Lins e Lins Silva
* Full paper name:
    *On GPU Connected Components and Properties: A Systematic Evaluation of Connected Component
    Labeling Algorithms and Their Extension for Property Extraction*
* Published in: *IEEE Transactions on Image Processing*, 2019, volume 28, number 1, pages 17-31 (ISSN 1057-7149)
* DOI: 10.1109/TIP.2018.2851445
* `IEEEXplore link <http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8399868&isnumber=8468142>`_
* Keywords: Graphics processing units, Kernel, Covariance matrices, Labeling, Computational
  modeling, Memory management, Merging, Parallel processing, region growing, labeling, image
  analysis, connected components, graphics processors
* :download:`Full text <./document.pdf>`. **Note:** this is the author-submitted manuscript version!
  Check the `IEEEXplore link`_ for the latest version, which includes improved figures and updated
  table data.
* Citation in :download:`BibTeX format <./citation.bib>`.
* Source-code: https://gitlab.com/lcg/cuccl.

