..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_notes:

Notes
=====

This section contains my notes (including a lot of Jupyter_ notebooks) on various topics, a sort of
personal knowledge base or wiki. This content is dynamic, sketchy, and ever-changing, unlike the
posts in the :ref:`blog-archives` section, which receive updates parsimoniously.

.. toctree::
   :maxdepth: 1
   :glob:

   */index
   *
   
.. _Jupyter: https://jupyter.org

