..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_coding:

===========
Source-code
===========

Here you'll find some of the open-source projects I have authored/contributed to. I prefer to keep
my repositories in `Gitlab <https://gitlab.com>`_, rather than the most popular `Github
<https://github.com>`_ for practical and historical reasons: Gitlab_ provides a powerful all-in-one
*Devops* solution, known as `Gitlab CI <https://about.gitlab.com/product/continuous-integration/>`_;
and it used to provide free private repositories, while Github_ only started doing so in 2019 [1]_.

.. contents:: Table of contents:
   :local:

.. [1] https://github.blog/2019-01-07-new-year-new-github/

Authoring
=========

Papers
------

* `cuccl <https://gitlab.com/lcg/cuccl>`_ -- code for the
  :ref:`paper_2019_on_gpu_connected_components_and_properties` paper, in which my colleagues and I
  developed an evaluation methodology for connected component labeling algorithms in GPUs. Uses C++
  11 and CUDA 7.5.

Studies
-------

* `hand-tracking <https://gitlab.com/psa-exe/hand-tracking/tree/9182812d>`_ -- the code for my
  :ref:`M.Sc. thesis <thesis_2016_estimating_hand_poses_from_rgb-d_data>`, a Particle Swarm
  Optimization-based human hand tracker that uses RGB-D frames from a *Kinect* camera to track a
  26-degree of freedom performing hand. Uses C++ 11 and CUDA 7.5.

Templates
---------

* `python-app <https://gitlab.com/pedroasad.com/templates/python/python-app>`_ -- template Python
  application, featuring automatic code styling, testing, documentation deploys, and other *Devops*
  facilities.
* `python-scibench <https://gitlab.com/pedroasad.com/templates/python/python-scibench>`_ -- template
  Python package for scientific projects, based on python-app_, that also includes support for
  `Jupyter <https://jupyter.org>`_ notebooks and a variety of scientific Python packages and tasks
  out of the box.

Tools
-----

* `python-pre-commit <https://gitlab.com/psa-exe/python-pre-commit>`_ -- a set of Python-based
  `pre-commit <https://pre-commit.com>`_ hooks for automating common checks when committing code in
  `Git <https://git-scm.com/>`_ repositories.
* `sphinx-thesis <https://gitlab.com/psa-exe/sphinx-thesis>`_ -- extension plugin for Python's
  documentation system (`Sphinx <http://sphinx-doc.org/>`_) that allows to write academic
  documents/documentation, including documented experimental notebooks, theses, and papers.


Contributing
============

Coming soon...

.. bibliography:: /publications/2019/01_on_gpu_connected_components_and_properties/citation.bib

