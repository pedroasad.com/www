Unsorted notes
==============

Bibliography
------------

Digital signal processing
~~~~~~~~~~~~~~~~~~~~~~~~~

Books and papers
^^^^^^^^^^^^^^^^

-  Computacional Neuroscience

   -  `Read
      online <http://www.fulviofrisone.com/attachments/article/412/Computational%20Neuroscience.pdf>`__

-  The Scientist & Engineer's Guide to Digital Signal Processing (1999)

   -  `Read online <http://www.dspguide.com/whatdsp.htm>`__ or `download
      it <http://www.analog.com/media/en/technical-documentation/dsp-book/dsp_book_frontmat.pdf>`__
      for free

-  Digital Signal Processing: Principles, Algorithms, and Applications

   -  `PDF <http://ultra.sdk.free.fr/docs/DxO/Digital%20Signal%20Processing%20-%20Principles,%20Algorithms%20&%20Applications%20Proakis%20&%20Dimitris%20G.%20Manolakis.pdf>`__
   -  This is the book we have in our library

Online courses
^^^^^^^^^^^^^^

-  `Digital Signal Processing <https://pt.coursera.org/learn/dsp>`__ on
   Coursera

Statistical methods
~~~~~~~~~~~~~~~~~~~

Books and papers
^^^^^^^^^^^^^^^^

-  Statistical Methods

   -  `Get
      it <https://www.sciencedirect.com/science/book/9780123749703>`__
      for free in ScienceDirect

-  Elements of Statistical Learning

   -  `Amazon <https://www.amazon.com/Elements-Statistical-Learning-Prediction-Statistics/dp/0387848576/ref=pd_sim_14_3?_encoding=UTF8&pd_rd_i=0387848576&pd_rd_r=7F1BT1WJBW8RR8D45DG2&pd_rd_w=fCczK&pd_rd_wg=feyce&psc=1&refRID=7F1BT1WJBW8RR8D45DG2&dpID=41aQrQaPseL&preST=_SY291_BO1,204,203,200_QL40_&dpSrc=detail>`__

-  Bayesian Data Analysis

   -  `Amazon <https://www.amazon.com/Bayesian-Analysis-Chapman-Statistical-Science/dp/1439840954/ref=pd_sim_14_2?_encoding=UTF8&pd_rd_i=1439840954&pd_rd_r=7F1BT1WJBW8RR8D45DG2&pd_rd_w=fCczK&pd_rd_wg=feyce&psc=1&refRID=7F1BT1WJBW8RR8D45DG2>`__

-  Fundamentals of Computational Neuroscience

   -  `Amazon <https://www.amazon.com.br/Fundamentals-Computational-Neuroscience-Thomas-Trappenberg/dp/0199568413>`__

Online courses
^^^^^^^^^^^^^^

-  `Statistical
   Inference <https://www.coursera.org/learn/mcmc-bayesian-statistics>`__
   on Coursera
-  `Inferential
   Statistics <https://www.coursera.org/learn/inferential-statistics-intro>`__
   on Coursera

Information theory
~~~~~~~~~~~~~~~~~~

Books and papers
^^^^^^^^^^^^^^^^

-  Information Theory and Network Coding

   -  `PDF <http://iest2.ie.cuhk.edu.hk/~whyeung/post/draft2.pdf>`__
   -  This is the book used in the Coursera course below

-  Seminal paper of information theory

   -  `PDF <http://math.harvard.edu/~ctm/home/text/others/shannon/entropy/entropy.pdf>`__
      (1948)

-  The Mathematical Theory of Communication

   -  Extended version of the seminal paper
   -  `PDF <https://www.amazon.com/Mathematical-Theory-Communication-Claude-Shannon/dp/0252725468/ref=sr_1_2?ie=UTF8&qid=1297150323&sr=8-2>`__

-  Elements of Information Theory

   -  `Amazon <https://www.amazon.com/Elements-Information-Theory-Telecommunications-Processing/dp/0471241954/ref=pd_bxgy_b_img_c>`__

Online courses
^^^^^^^^^^^^^^

-  `Information
   Theory <https://www.coursera.org/learn/information-theory>`__ on
   Coursera

Computational neuroscience
~~~~~~~~~~~~~~~~~~~~~~~~~~

Books
^^^^^

-  Computational Neuroscience: A First Course

   -  `Amazon <https://www.amazon.com/Computational-Neuroscience-Course-Springer-Neuroinformatics/dp/3319008609>`__

-  Computational Modeling Methods for Neuroscientists

   -  `Amazon <https://www.amazon.com/Computational-Modeling-Methods-Neuroscientists-Neuroscience/dp/0262013274>`__

-  Neural Engineering: Computation, Representation, and Dynamics in
   Neurobiological Systems

   -  `Amazon <https://www.amazon.com/Neural-Engineering-Representation-Neurobiological-Computational/dp/0262550601#reader_0262550601>`__

-  Lecture notes by Todd Troyer

   -  `PDF <http://www.utsa.edu/troyerlab/compneuronotes.pdf>`__

Online courses
^^^^^^^^^^^^^^

-  `Computational
   Neuroscience <https://pt.coursera.org/learn/computational-neuroscience>`__
   on Coursera
-  `Synapses, Neurons, and
   Brains <https://www.coursera.org/learn/synapses>`__ on Coursera
-  `Visual Perception and the
   Brain <https://pt.coursera.org/learn/visual-perception>`__ on
   Coursera

Other possibly interesting topics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Books
-----

-  Pattern Recognition and Machine Learning

   -  `PDF <http://users.isr.ist.utl.pt/~wurmd/Livros/school/Bishop%20-%20Pattern%20Recognition%20And%20Machine%20Learning%20-%20Springer%20%202006.pdf>`__

-  How to Build a Brain: A Neural Architecture for Biological Cognition
-  The Developing Mind, Second Edition: How Relationships and the Brain
   Interact to Shape Who We Are

Notes
-----

-  `Road Map for Choosing Between Statistical Modeling and Machine
   learning <http://www.fharrell.com/post/stat-ml/>`__
-  `Statistics course by PennState Eberly College of
   Science <https://onlinecourses.science.psu.edu/stat500/node/1/>`__

   -  `How to determine which statistical method to use for a specific
      situation <https://onlinecourses.science.psu.edu/stat500/sites/onlinecourses.science.psu.edu.stat500/files/lesson12/correct_technique/index.png>`__

-  MAP vs. MLE

   -  Wikipedia articles

      -  `MLE <https://en.wikipedia.org/wiki/Maximum_likelihood_estimation>`__
      -  `MAP <https://en.wikipedia.org/wiki/Maximum_a_posteriori_estimation>`__

   -  A short `blog
      post <https://wiseodd.github.io/techblog/2017/01/01/mle-vs-map/>`__
      about it
