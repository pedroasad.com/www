.. vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

Packaging, dependency management, and publishing
================================================

* `A survey of package management tools in the Python ecosystem <https://github.com/cs01/python-packaging-tools>`__ (by Facebook Software Engineer `Chad Smith <https://grassfedcode.com/>`__)
* `Managing Application Dependencies <https://packaging.python.org/tutorials/managing-dependencies/>`__ (from the `Python Packaging User Guide <https://packaging.python.org/>`__)
* `Pipenv: A Guide to the New Python Packaging Tool <https://realpython.com/pipenv-guide/>`__ (by Developer `Alexander VanTol <https://www.alexandervt.com/blog/>`__)
* `Application Dependencies <https://www.fullstackpython.com/application-dependencies.html>`__

