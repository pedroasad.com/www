.. vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

Python references
=================

* `The Hitchhiker’s Guide to Python! <https://docs.python-guide.org/>`_ --- a frequently updated e-book (also an O'Reilly book) that aims to "[...] provide both novice and expert Python developers a best practice handbook to the installation, configuration, and usage of Python on a daily basis".
* `RealPython <https://realpython.com/>`_ --- a community and knowledge base of experienced Python programmers that produces high quality tutorials, from the very basics, to Python internals, and web development, on a weekly basis.
  Contains some paid courses, but a lot of the material is available for free.

