..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

===================
Functional analysis
===================

A series of studies about mathematical functions.

.. toctree::
   :maxdepth: 1
   :numbered:
    
   convolution-correlation.ipynb

