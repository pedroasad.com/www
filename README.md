<!--
   vim: et: tw=0:
   vim: filetype=markdown:
   vim: spell: spelllang=en:
-->

# Pedro Asad's public knowledge base, notes, and website

[![][pipeline-status]][repository-master]

[![][binder-logo]][binder-link]

[![][cc-logo]][CC By-SA 4.0]

* Built with [Sphinx][sphinx]
* Deployed wih [GitLab CI/CD][ci]
* Accessible at https://pedroasad.com

---

This repository contains my personal web page, notes, and knowledge base (including a collection of [Jupyter][jupyter] Python) notebooks (previously known as _IPython notebooks_) that are automatically published to my [website] through [GitLab CI][ci].
They are glued together by [Sphinx][sphinx]-generated documentation pages, and their dependencies managed via [Poetry][poetry].

All content is licensed under a [CC By-SA 4.0] *open culture* license, included in the [LICENSE.txt](LICENSE.txt) file.
I chose this license mainly because this website contains educational material and notebooks that I want to be freely usable by students.
If you want to create your own personal website based on this repository, the license allows you to do just that, but almost needless to say: you should not forget to the content of identity-related information (like the Curriculum and About sections), for moral reasons.
Also, do not forget to cite the original work.

Read on for instructions on how to start using this repository.

---


## Instructions

**Dependencies:**

* Git
* Python &ge; 3.6
* [Poetry] &ge; 1.0
* [pre-commit]

To begin with, install [Poetry][poetry].
The recommended way of achieving this is via:

```bash
curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
```

[Poetry][poetry] can automatically create a virtualenv for you, but you may also choose to manage
virtualenv creation by setting

```bash
poetry config settings.virtualenvs.create false
```

So **either** make sure to prepend the following commands (except for `pre-commit`) with `poetry run` (in case you're using automatic virtualenv creation), **or** activate the target virtualenv before the following steps (otherwise).

Install dependencies and perform further configuration with

```bash
poetry install
pre-commit install
poetry run jupyter contrib nbextension install --sys-prefix
```

This will install all dependencies, including useful development packages, and set up Jupyter extensions.
For instance, the `sphinx-autobuild`, which automatically rebuilds the documentation on source changes, will be installed.

## Usage

At this point, building will be as simple as running

```bash
poetry run make html
```

which will build the HTML docs to `build/html`.
If you instead call `make livehtml`, then `sphinx-autobuild` will build the pages in the same directory, but also serve them under `http://127.0.0.1:9876` (you can change the port by defining the `AUTOBUILDPORT` environment variable), and actively rebuild the whole site if any changes are made to source files.
Together with

```bash
poetry run jupyter notebook
```

this sets up a perfectly functional and comfortable authoring environment!


## Contents

The repository is organized as follows:

* [`.dockerignore`](.dockerignore) - Files ignored by [Binder] when serving notebooks
* [`.pre-commit-config.yaml`](.pre-commit-config.yaml) - [pre-commit] configuration file
* [`LICENSE.txt`](LICENSE.txt) - Copy of the [CC By-SA 4.0] license text
* [`Makefile`](Makefile) - Recipies for building the website
* [`README.md`](README.md) - This file
* [`pyproject.lock`](pyproject.lock) - Resolved dependency graph
* [`pyproject.toml`](pyproject.toml) - List of dependencies/package description
* [`requirements.txt`](requirements.txt) - Requirements file (automatically exported by [pre-commit] in order to match [`pyproject.toml`](pyproject.toml)-specified dependencies)
* [`source`](source) - Source tree
  * [`about.rst`](`source/about.rst`) - [About page][website-about]
  * [`conf.py`](`source/conf.py`) - Sphinx configuration file
  * [`curriculum.rst`](`source/about.rst`) - [Curriculum page][website-curriculum]
  * [`index.rst`](`source/index.rst`) - [Welcome page][website-home]
  * [`notes`](`source/notes`) - [Notes section][website-notes]
  * [`posts`](`source/posts`) - Blog posts, categorized in subdirectories by date and subject
  * [`publications`](`source/publications`) - [Publications section][website-publications]
  * [`_static`](`source/_static`) - Assets (image files, etc.)
  * [`teaching`](`source/teaching`) - [Teaching section][website-teaching]
    * [`_templates`](`source/_templates`) - Modifications of default [Sphinx] templates

All notebooks `notebook.ipynb` are automatically converted to `notebook.ipynb.rst` by `make <target>`.
Hence, when referring to notebook files in Sphinx pages (in `toctree`, for instance), the referenced file name is indeed `notebook.ipynb`.
This approach is preferred to using the [nbsphinx][nbsphinx] because it allows to write blog posts in notebook files directly, without having to put them in another file's toctree, a current limitation of the [ABlog][ablog] + [nbsphinx][nbsphinx] combination.

__Note:__ always save your notebooks the way you want them to be exported.
It is assumed that you may have results depending on random/outer factors and that [CI][ci] time should be preserved, so the notebooks won't be run again before converting to HTML.


## Notes

### Notebook instructions

When creating notebooks containing [`ipywidgets`][ipywidgets], the notebook must contain a setup code block like

```jupyter
import matplotlib
%matplotlib inline
```

and you must also remember to use the `Widgets -> Save Notebook Widget State` command from the notebook's menu bar. **Don't use** `%matploblib notebook` as this will likely result in an error message like `JavaScript output is disabled in JupyterLab` displayed where the embedded widget should be.


## CI Pipeline

The [CI][ci] pipeline depends on a couple of [secret variables][secret-variables]:

                   
| Variable name   | Meaning                              | How to set it (summary)                  |
|-----------------|--------------------------------------|------------------------------------------|
| `SSH_HOST`      | Name/IP of target host               |                                          |
| `SSH_HOST_KEY`  | SSH key of target host               | `ssh-keyscan -p PORT HOST`               |
| `SSH_PORT`      | Target host's SSH port               | Usually 22                               |
| `SSH_USER`      | User with SSH access in target host  |                                          |
| `SSH_KEY`       | User's private key                   | `ssh-keygen -r rsa -t key_file`          |
| `HOST_DEST_DIR` | Destination directory in target host | Something like `/var/www/html/notebooks` |


The host key is obtainable with

```bash
ssh-keyscan -p PORT HOST
```

The public/private key-pair may be generated  with

```bash
ssh-keyscan -t rsa -C "Comment" -f key_file
```

but no passphrase should be supplied.
The contents of the generated private key (`key_file` file) should be pasted into the `SSH_KEY` secret variable, and the contents of the public key (`key_file.pub` file) should be put in the list of user-authorized keys.
This is usually by accomplished by running 

```bash
cat key_file.pub >>~/.ssh/authorized_keys
```

on the target host after transferring the public key to it.
You may also use a pre-existing private/public key pair, if you which.
Make sure to lock away the `key_file` private key file in a safe place with proper permissions after performing this setup, or get rid of it completely.
You can read more about setting up SSH authentication [here][ssh-config].

[Binder]: https://ovh.mybinder.org
[CC By-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/legalcode
[ablog]: https://ablog.readthedocs.io/
[binder-link]: https://mybinder.org/v2/gl/pedroasad.com%2Fwww/master
[binder-logo]: https://mybinder.org/badge_logo.svg
[cc-logo]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[ci]: https://about.gitlab.com/gitlab-ci/
[direnv]: https://direnv.net
[ipywidgets]: https://ipywidgets.readthedocs.io
[jupyter]: http://jupyter.org
[nbsphinx]: https://nbsphinx.readthedocs.io/en/latest
[pipeline-status]: https://gitlab.com/psa-exe/pedroasad.com/badges/master/pipeline.svg
[poetry]: https://github.com/sdispater/poetry
[pre-commit]: https://pre-commit.com/
[repository-master]: https://gitlab.com/psa-exe/pedroasad.com/commits/master
[secret-variables]: https://docs.gitlab.com/ee/ci/variables/
[sphinx]: http://www.sphinx-doc.org/
[ssh-config]: https://www.ssh.com/ssh/key/#sec-How-to-configure-key-based-authentication
[website]: https://pedroasad.com
[website-home]: https://pedroasad.com
[website-about]: https://pedroasad.com/about.html
[website-curriculum]: https://pedroasad.com/curriculum.html
[website-notes]: https://pedroasad.com/notes/index.html
[website-publications]: https://pedroasad.com/publications/index.html
[website-teaching]: https://pedroasad.com/teaching/index.html

