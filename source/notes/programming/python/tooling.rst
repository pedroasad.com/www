Python tooling
==============

* `Python tooling makes a project tick <https://medium.com/georgian-impact-blog/python-tooling-makes-a-project-tick-181d567eea44>`_, by Adithya Balaji.
* `10 steps to set up your Python project for success <https://towardsdatascience.com/10-steps-to-set-up-your-python-project-for-success-14ff88b5d13>`_, by Jeff Hale.
