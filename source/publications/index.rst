..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_publications:

============
Publications
============

This page lists my publications. Each publication links to a page about it, where you'll find a
link to the online version, a downloadable citation in BibTeX format, and (hopefully) a brief
explanation about.

.. note::
   I prefer not to provide downloadable manuscripts because they are usually changed in sensitive
   ways during the final publication process. If you would like to read one of my papers and
   have no alternative (I know, journal subscriptions are expensive) for accessing the journal's
   published version, `email me <mailto:pasad@lcg.ufrj.br>`_ and I'll send you the manuscript.

2019
====

.. toctree::
   :glob:
   
   2019/**

2016
====

.. toctree::
   :glob:

   2016/**

