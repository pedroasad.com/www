========================
Python 3 design patterns
========================

Interesting links
=================

* `Three techniques for inverting control, in Python <https://seddonym.me/2019/08/03/ioc-techniques/>`_, by David Seddon
  --- covers the basic of how to handle control flow to to configurable plugins using either: dependency injection, observer pattern, or monkey patching.
* `Design patterns in Python <https://refactoring.guru/design-patterns/python>`_, by the `Refactoring Guru <https://refactoring.guru>`_ 
  --- a catalogue of classic design patterns (read GoF_ patterns).
* `Python patterns <https://python-patterns.guide/>`_, by Brandon Rhodes
  --- a catalogue of design patterns (classified according to the GoF_ terminology), including Python-specific patterns, with code examples.
* `python-patterns <https://github.com/faif/python-patterns>`_ --- a Github repository of documented design pattern (GoF_ or otherwise) implementations in Python.

.. _GoF: https://en.wikipedia.org/wiki/Design_Patterns
