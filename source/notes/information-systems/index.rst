===================
Information systems
===================

REST --- Representational State Transfer
========================================

**References:**

* `REST Wikipedia`_ page.
* Brief `historical overview <REST history_>`_ on how REST came to be.
* `Ph.D. thesis <REST thesis_>`_ that proposed the REST architecture, by `Roy Thomas Fielding`_.

.. _REST history: http://greglturnquist.com/2016/05/03/rest-soap-corba-e-got/
.. _REST thesis: https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm
.. _REST Wikipedia: https://en.wikipedia.org/wiki/Representational_state_transfer
.. _Roy Thomas Fielding: http://roy.gbiv.com/
