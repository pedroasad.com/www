..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_about:

About
=====

My name is Pedro Asad, and I am a doctoral student at the `Computer Graphics Laboratory <lcg_>`_
(LGC), part of the `Federal University of Rio de Janeiro <ufrj_>`_'s (UFRJ) `Computer and Systems
Engineering Graduate Program <pesc_>`_ (PESC). I am currently researching how to apply scientific
visualization to neuroscience experiments, investigating a large dataset of electrophysiological
recordings in the primate `visual cortex <wikipedia-visual-cortex_>`_, produced by the `Laboratory
of Cognitive Physiology <lfc_>`_ of my university's `biophysics department <ibccf_>`_.

I am also familiar with the fundamentals of contemporary image processing, computer vision, and GPU
programming from my M.Sc. days, during which I developed a `Particle Swarm
<wikipedia-particle-swarm_>`_-based a hand tracking system using the Kinect's RGBD frames as input.
I also taught introductory programming courses to engineering students as a temporary teacher of my
university's `Computer Science Department <dcc_>`_ (DCC) from March 2015 to March 2017.  You can
access my full Lattes CV [1]_ `here <lattes_cv_>`_.

My main programming languages are Python 3, C++, and CUDA, and I occasionally dive into JavaScript.
Of course, if duty calls, I can get around Matlab, SQL, etc. that is, the languages most commonly
taught to CS bachelors. I am currently delving into market technologies like React.js, Docker, Tox,
Flask and Gitlab's continuous integration in order to improve things like quality, reliability,
reproducibility, and presentation of my projects. That is because I believe we, CS researchers,
should do our best to counter the occasional impression that scientific software is unusable and
looks like it was made in the 90's, something that is starting to change gradually.

And what is this page for?
--------------------------

I have been studying many topics and developing tons of code since my bachelor days, from 2008 to
2013, but unfortunately, as many other young researchers, it took me some time to realize how
important it is to take my time to write a proper research page in order to report my findings and
accomplishments, and to share ideas with fellow researchers. As a result, I own numerous private
repositories in `Gitlab <gitlab_>`_, `Github <github_>`_, and `Bitbucket <bitbucket_>`_, including
a lot of `Jupyter <jupyter_>`_ Python notebooks, but most of this code still needs some polishing
before it can be properly published for the benefit of others.

My intent with this page is to proggressively publish past and ongoing work and to, as frequent as
possible, write about useful topics that lie approximately in the intersection of:

 * computer science
 * science in general
 * research
 * postgraduate life
 * attended conferences
 * scientific writing and publishing
 * programming and tools for science
 
.. include:: pointers.rst

----

.. [1]
   The *Lattes Curriculum* is the standard curriculum for all Brazilian researchers, either students
   and professors, funded by the Brazilian Government. More information can be found in it `this
   page <lattes_>`_.

.. _bitbucket: https://bitbucket.org
.. _dcc: https://dcc.ufrj.br
.. _github: https://github.com
.. _gitlab: https://gitlab.com
.. _ibccf: http://www.biof.ufrj.br
.. _jupyter: http://jupyter.org/
.. _lattes: http://lattes.cnpq.br
.. _lattes_cv: http://lattes.cnpq.br/1991114698553806
.. _lcg: https://www.lcg.ufrj.br
.. _lfc: http://www.biof.ufrj.br/pt-br/node/44
.. _pesc: https://www.cos.ufrj.br
.. _ufrj: https://ufrj.br
.. _wikipedia-particle-swarm: https://en.wikipedia.org/wiki/Particle_swarm_optimization
.. _wikipedia-visual-cortex: https://en.wikipedia.org/wiki/Visual_cortex

