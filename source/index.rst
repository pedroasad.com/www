..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_home:

====
Home
====

Hi! I am a doctoral student at the `Computer Graphics Laboratory <lcg_>`_ (LGC), at the `Federal
University of Rio de Janeiro <ufrj_>`_ (UFRJ), and I am currently studying how to apply data
visualization to neuroscience experiments in the non-human primate `visual cortex
<wikipedia-visual-cortex_>`_. Read more about myself and the goals of this website in the
:ref:`About section <section_about>`.

.. include:: pointers.rst

.. toctree::
   :hidden:
   :maxdepth: 1
    
   about
   curriculum
   coding/index
   notes/index
   publications/index
   teaching/index

Recent posts
============
.. postlist:: 5
   :excerpts:

.. _lcg: https://www.lcg.ufrj.br
.. _ufrj: https://ufrj.br
.. _wikipedia-visual-cortex: https://en.wikipedia.org/wiki/Visual_cortex

