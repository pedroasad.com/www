..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _notes_dsp:

=========================
Digital Signal Processing
=========================

Presentation
============

 * `Course presentation lecture <https://www.coursera.org/learn/dsp/lecture/rrFX4/welcome-to-the-dsp-course>`_.

Course modules
==============

.. toctree::
   :maxdepth: 2
   :numbered:
    
   module1

Bibliography
============

 * An Introduction to Hilbert Space
    * `Amazon link <_hilbert-space-book>`_
 * Signal Processing for Communications
    * Available online for free
       * `HTML <http://www.sp4comm.org/webversion.html>`_
       * `PDF <http://www.sp4comm.org/docs/sp4comm_corrected.pdf>`_
    
.. _hilbert-space-book: https://www.amazon.com/Introduction-Hilbert-Cambridge-Mathematical-Textbooks/dp/0521337178
