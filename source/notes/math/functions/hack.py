import IPython.display
import ipywidgets
import functools


def interact_update_dict_run_tags(symbols, tags, **kwargs):
    @run_tags_after(tags)
    @update_dict(symbols)
    def callback(**kwargs):
        pass
    return ipywidgets.interact(callback, **kwargs)


def _run_tags(tags, when):
    """Implementation of :func:`run_tags_after` and :func:`run_tags_before`.
    """
    def decorator(callback):
        @functools.wraps(callback)
        def decorated(**kwargs):
            javascript = '''
            (function() {
                var cells = Jupyter.notebook.get_cells();
                var tags = %r;
                for (var i = 0; i < cells.length; i++) {
                    for (var j = 0; j < tags.length; j++) {
                        if (cells[i].metadata.tags && cells[i].metadata.tags.indexOf(tags[j]) !== -1) {
                            console.log('Executing cell', i);
                            Jupyter.notebook.select(i)
                            Jupyter.notebook.execute_cell();
                        }
                    }
                }
            })();
            '''
            if when == 'after':
                result = callback(**kwargs)
                IPython.display.display(
                    IPython.display.Javascript(javascript % tags)
                )
            elif when == 'before':
                IPython.display.display(
                    IPython.display.Javascript(javascript % tags)
                )
                result = callback(**kwargs)
            else:
                raise ValueError("Invalid value for argument 'when': %r" % when)
            return result
        return decorated
    return decorator


def run_tags_after(tags):
    """Creates a decorator for ipywidgets callbacks that runs tagged cells.
    
    This function returns a decorator that wraps a callback passed to 
    :func:`ipywidgets.interact` so that, after running the callback, all notebook
    cells containing one of the specified tags are re-executed.
    
    Parameters
    ----------
    tags: sequence of tags
    
    Example
    -------
    If you have a notebook with code that looks like this
    
    .. code:: python
        
        import ipywidgets as ipyw
        
        
        def function(a, b):
            print(a, b)
            
        
        ipyw.interact(function, a=9, b=3.0)
        
    Then you could wrap it with
    
    .. code:: python
    
        @run_tags_after(['my_tag', 'another_tag'])
        def function(a, b):
            print(a, b)
            
    in order to update all cells containing at least one of the `my_tag`, or
    `another_tag` tags after running the callback, whenever the interactive
    controls for `a` and `b` are manipulated.
    
    See also
    --------
    :func:`run_tags_before`
    """
    return _run_tags(tags, when='after')
                
                
def run_tags_before(tags):
    """Creates a decorator for ipywidgets callbacks that runs tagged cells.
    
    This function returns a decorator that wraps a callback passed to 
    :func:`ipywidgets.interact` so that, before running the callback, all notebook
    cells containing one of the specified tags are re-executed.
    
    Parameters
    ----------
    tags: sequence of tags
    
    Example
    -------
    If you have a notebook with code that looks like this
    
    .. code:: python
        
        import ipywidgets as ipyw
        
        
        def function(a, b):
            print(a, b)
            
        
        ipyw.interact(function, a=9, b=3.0)
        
    Then you could wrap it with
    
    .. code:: python
    
        @run_tags_after(['my_tag', 'another_tag'])
        def function(a, b):
            print(a, b)
            
    in order to update all cells containing at least one of the `my_tag`, or
    `another_tag` tags before running the callback, whenever the interactive
    controls for `a` and `b` are manipulated.
    
    See also
    --------
    :func:`run_tags_after`
    """
    return _run_tags(tags, when='before')


def update_dict(symbols):
    """Creates a decorator for ipywidgets callbacks that updates symbol tables.
    
    This function returns a decorator that wraps a callback passed to 
    :func:`ipywidgets.interact`, updating all interaction key-value pairs into the 
    dictionary passed, prior to executing the callback. A useful trick is passing
    `symbols=globals()`, so that global variables in the caller module are updated.
    
    Parameters
    ----------
    symbols: dict
        A dictionary that will be updated with key-value pairs before running the
        wrapped callback.
    """
    def decorator(callback):
        @functools.wraps(callback)
        def decorated(**kwargs):
            for key, val in kwargs.items():
                symbols[key] = val
            return callback(**kwargs)
        return decorated
    return decorator
