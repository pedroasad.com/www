..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_notes_neuroscience_neural-coding:

Neural coding
=============

.. toctree::
   :maxdepth: 2
   :glob:

   *

