..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_notes_programming:

Programming
===========

.. toctree::
   :maxdepth: 2

   python/index

Interesting pages
-----------------

* `Reginald Braithwaite`_ --- this guy has lots of interesting talks covering programming concepts and design patterns, specially on JavaScript.


.. _Reginald Braithwaite: https://raganwald.com/

