..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

.. _section_teaching:

========
Teaching
========

.. note::
    This page is currently under reconstruction. My lecture materials were hosted on a previous,
    untracked version of this website, some of it inside a Moodle_. I didn't back up Moodle's
    database correctly when I shut down the previous site in order to start this new one. As a
    consequence, I'm having to retrieve the contents and reorganize them into course pages one by
    one, not to mention some other hard-coded HTML pages that I'm studying how to integrate
    seamlessly into the current Sphinx_-based website. Pardon me if you came here looking for those
    materials! I hope to get everything back in place by December 2018, please be patient and come
    back later.

.. contents::
    :local:

2015
====

First semester
--------------

Second semester
---------------

2016
====

First semester
--------------

Second semester
---------------

.. _Moodle: https://moodle.org
.. _Sphinx: http://sphinx-doc.org/

