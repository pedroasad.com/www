..
   vim: et: sw=3: ts=3: tw=0:
   vim: filetype=rst:
   vim: spell: spelllang=en:

============
Introduction
============

.. contents::
   :local:

Introduction to digital signal processing
=========================================

It starts with `this lecture <https://www.coursera.org/learn/dsp/lecture/ioZFl/1-1-a-introduction-to-digital-signal-processing>`_.

Discretization of time
----------------------

Discrete models are simpler to work with from a computational perspective, but are we loosing power?
The answer to this question is given to us by the *Sampling Theorem*, stated in 1920 by Harry Nyquist and Claude Shannon.
Any continuous signal :math:`x(t)` may be reconstructed from a discrete set of samples :math:`\{x_i\}`,

.. math::

   x(t) = \sum_{i=-\infty}^{\infty} x_i \, \rm{sinc} \left( \frac{t - i\Delta t}{\Delta t} \right)

provided that the signal's shape changes *slowly enough*. That imposes a a size restriction on the sampling interval :math:`\Delta t` that may be calculated  through Fourrier analysis in order to obtain the largest $\Delta t$ that preserves all information.

Discretization of amplitude
---------------------------

Brings advantages to three domains:

* Storage
* Transmission
* Processing

With discretized values (even floating-point is discretized, after all), we may use general purpose memory and processors to store many types of signals. Compared to phonographs, photographs, and temperature registration devices, this is a huge advancement. Furthermore, analog transmission processes inherently introduce two undesirable factors into a transmitted signal: noise and attenuation.

Discrete time signals
=====================

`Lecture <https://www.coursera.org/learn/dsp/lecture/sWofV/1-2-a-discrete-time-signals>`_.

Basic signal processing
=======================

Watch the `intro video <https://www.coursera.org/learn/dsp/lecture/ORbNE/1-3-a-how-your-pc-plays-discrete-time-sounds>`_. It os followed by a `presentation <https://www.coursera.org/learn/dsp/lecture/NpNjI/1-3-b-the-karplus-strong-algorithm>`_ of the Karplus-Strong algorithm. This `link <https://ipython-books.github.io/117-creating-a-sound-synthesizer-in-the-notebook/>`_ tells how to synthesize sounds with an IPython notebook. The `last lecture <https://www.coursera.org/learn/dsp/lecture/fa1sI/signal-of-the-day-goethes-temperature-measurement>`_ presents a slightly more involved example of recursive signals and their corresponding block diagrams: one for computing sliding averages. The interesting thing is that by algebraically manipulating the formula, we arrive at a block diagram with considerably smaller memory requirements, which is bery good in terms of fabrication.

Complex exponentials
====================

`This video <https://www.coursera.org/learn/dsp/lecture/Z437t/1-4-a-complex-exponentials>`_ recalls the exponential notation for sinusoids and introduces the aliasing problem. Reminds me of my study of the discrete Fourier transform, prepared with my friend Thalles back in 2010 for the quantum computing special lecture with prof. Collier.

TODO
====

 * Write down my back-of-the-envelope notes
 * `Reading list <https://www.coursera.org/learn/dsp/supplement/eYNqf/notes-and-external-resources>`_
 * Explore and fork notebooks
    * `Transoceanic Signal Transmission <https://www.coursera.org/learn/dsp/notebook/FkamJ/transoceanic-signal-transmission>`_
    * `Karplus-Strong algorithm <https://www.coursera.org/learn/dsp/notebook/4IqPe/the-karplus-strong-algorithm>`_
 * Homework and assignments
    * `Practice homework <https://www.coursera.org/learn/dsp/supplement/CzA5P/practice-homework-for-module-1>`_
    * `Test for module 1 <https://www.coursera.org/learn/dsp/exam/E1qo7/homework-for-module-1>`_
